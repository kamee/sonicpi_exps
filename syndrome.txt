5.times do
  use_synth :fm
  with_fx :echo do
    play 50, pan: 1
    sleep 0.5
    sample :elec_plip
    sleep 0.5
    play 62
  end
  sleep 0.5
  with_fx :echo do
    play 50, pan: -1
    sleep 0.5
    sample :elec_plip
    sleep 0.5
    play 62
  end
end